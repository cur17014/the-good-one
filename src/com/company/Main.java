package com.company;

import java.util.*;

public class Main {

// These imports allow our java collections to be used
// more info here https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html

    // Public static void main is used to run code in the console
    public static <T> void main(String[] args) {

        // Create a new empty array list with strings named 'l' the 'T' means that it is generic
        // and can be anything. This also means thats we will need to tell our list that we are adding
        // strings by casting them on lines 17-20
        List<String> l = new ArrayList<String>();

        // Add strings to our list
        l.add("James");
        l.add("Katie");
        l.add("Josh");



        // Loop to print out entire list
        System.out.println("Array List Contents:");
        for (int i = 0; i < l.size(); i++){
            System.out.println(l.get(i));

        }
        // With lists you can easily access things from the middle of it

        System.out.println("The item at index 1 in the list 'l' is: " + l.get(1));



    }
}
