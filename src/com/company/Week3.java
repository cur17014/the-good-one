package com.company;

/*import java.math.BigDecimal;
import java.math.RoundingMode;*/
import java.text.DecimalFormat;
import java.util.Scanner;

public class Week3 {

    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {

        // a. Create a program that gathers input of two numbers from the keyboard in its main
        // method and then calls the method below to calculate.
        System.out.println("Enter num1 to divide:");
        String num1 = in.nextLine();
        System.out.println("Enter num2 to divide:");
        String num2 = in.nextLine();

        int n1 = 0;
        int n2 = 0;

        try{

            n1 = Integer.parseInt(num1);
            n2 = Integer.parseInt(num2);

            // c. In your main method, use exception handling to catch the most specific exception possible.
            // Display a descriptive message in the catch to tell the user what the problem is.
        }catch(NumberFormatException e) {
            System.out.println("You have not entered valid numbers, please try again. " + e);

            try {
                // c. Give the user a chance to retry the data entry.
                System.out.println("Re-Enter numbers 1 & 2 to divide:");
                n1 = Integer.parseInt(in.nextLine());
                n2 = Integer.parseInt(in.nextLine());
            }catch(NumberFormatException ex) {

                // c. Display a message in the final statement.
                System.out.println("You've entered the input too many times, closing the program.");
                return;
            }
        }
        // b. Write a method that divides the two numbers together (num1/num2) and returns the result.
        // The method should include a throws clause for the most specific exception possible if the user enters zero for num2
        // d. Show the output when there is no exception
        System.out.println("The answer to " + num1 + "/" + num2 + " is: " + doDivision(n1, n2));
    }

    /*
     * Takes in a number num1 and divides it by num2
     * Throws and error if the numbers are 0 or are something other than a number
     */
    private static float doDivision(int num1, int num2) {
        try {
            if(num2 == 0) {
                throw new ArithmeticException();
            }
        }catch(ArithmeticException e) {

            // d. Show the output when there is no exception and when a zero in the denominator is used.
            System.out.println("Cannot divide by 0! Try again! " + e);

            try {

                // e. If the user does type a zero in the denominator, display a message and give the user a chance to retry.
                System.out.println("Re-Enter numbers 1 & 2 to divide:");
                num1 = Integer.parseInt(in.nextLine());
                num2 = Integer.parseInt(in.nextLine());
            } catch (NumberFormatException ex) {
                System.out.println(ex);
            } catch (ArithmeticException Aex) {
                System.out.println("You've entered the input too many times, closing the program.");
            }
        }

        return num1/num2;

        //Below here were attempts I made to get the exact answer.  The reason I did this was
        //because in my testing if I divided 5/2 I would only get 2 as an answer.  Below are my
        //two "succesful" attempts.  the BigDecimal actually worked and I have to give credit to
        //a friend who showed me how to use BigDecimal.  The other option I came up with after
        //piecing together various articles I found on google. But, it doesn't truly give you
        //the answer. It just adds to 00s after the ".".

/*        BigDecimal numb1 = new BigDecimal(String.valueOf(num1));
        BigDecimal numb2 = new BigDecimal(String.valueOf(num2));
        BigDecimal numb3;

        return numb1.divide(numb2, 3, RoundingMode.CEILING);*/


        //double i = num1 / num2;
        //DecimalFormat f = new DecimalFormat("##.00");
        //String ans = f.format(i);
        //return ans;

    }

}
