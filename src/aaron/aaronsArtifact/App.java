package aaron.aaronsArtifact;

import java.util.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.*;



/**
 * a. Write an application with two separate components.
 * The first is an HTTP server component that when called takes an object and converts it to JSON
 * using the Jackson JSON parser, and returns a block of JSON using HTTP.
 *
 * The second component is a client component.  It makes a call to the server component, gets the JSON it
 * returns, and then converts that using the Jackson JSON parser, into an object.
 *
 * b. These two components need to be written as two separate classes.
 *
 * c. Make sure you incorporate error handling and data validation into your programs,
 * including catching the most specific type of exceptions you can.
 */
public class App
{
    public static void main( String[] args )
    {
        // Create a new Player and some values to the Object.
        Player player1 = new Player();
        player1.setName("Aaron C.");
        player1.setRank(1);

        String[] friendsList = {"Alex Romero", "Jason Miller", "Mr.Rogers"};
        player1.setFriendsList(friendsList);

        Queue<String> downloadsPending = new PriorityQueue<String>();
        downloadsPending.add("Barbies Princess Adventure");
        downloadsPending.add("My Little Pony: Friendship is Magic");
        downloadsPending.add("AmongUs");
        player1.setDownloadQueue(downloadsPending);

        System.out.println("client returned: " + App.client(player1).toString());
    }

    /*
     * a. An HTTP server component that when called takes an object
     * converts it to JSON using the Jackson JSON parser, and returns
     * a block of JSON using HTTP
     */
    public static String HTTPServer(Object obj) {

        // Converts to JSON using the Jackson JSON parser(lines 38-45)
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        // Return a block of JSON using HTTP.
        System.out.println("Json Server string s is: " + s);
        return s;
    }

    /*
     * Client component
     */
    public static Object client(Object s) {

        ObjectMapper mapper = new ObjectMapper();

        // a. makes a call to the server component HTTPServer(HTTPobj)
        // get the JSON it returns (String json = HTTPServer(HTTPobj))
        String json = HTTPServer(s);

        // Converts that using the Jackson JSON parser, into an object and returns it (lines 91-99)
        Object jsonObj = null;

        try {
            jsonObj = mapper.readValue(json, Object.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return jsonObj;

    }
}
