package aaron.aaronsArtifact;

import java.util.Queue;

public class Player {

    private String name;
    private int rank;
    private String[] friendsList;
    private Queue<String> downloadqueue;

    // Name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // Rank
    public int getRank() {
        return rank;
    }
    public void setRank(int rank) {
        this.rank = rank;
    }

    // FriendsList
    public String[] getFriendsList() {
        return friendsList;
    }
    public void setFriendsList(String[] friendsList) {
        this.friendsList = friendsList;
    }

    // Download Queue
    public Queue<String> getDownloads() {
        return downloadqueue;
    }
    public void setDownloadQueue(Queue<String> DownloadQueue) {
        this.downloadqueue = DownloadQueue;
    }
}
